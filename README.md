##SimpleSpreadSheet 

Thiis a simple project for interviewing which main function is a simulate table sheet printing via console.

There are 2 versions for the design of the project

Version 1:Simple factory by executing Translator.GetResult

Version 2:Simple factory + Interpretor by executing  Translator.GetResultByInterpretor

here are the class diagram and console output

[class diagram]https://bitbucket.org/wjcking/simplespreadsheet/src/e45562597abd/SimpleSpreadsheet/class.jpg?at=master

[console output]https://bitbucket.org/wjcking/simplespreadsheet/src/e45562597abd/SimpleSpreadsheet/console.jpg?at=master


##Technology

Visual Studio 2017 C#

##For the assessment criteria

1.The design principles are simple factory and interpreter patterns.

2.The main algorithm is string slice.

3.Utilized an unit testing project.

