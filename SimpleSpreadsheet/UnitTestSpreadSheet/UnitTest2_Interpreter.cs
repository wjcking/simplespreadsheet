﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSpreadsheet;
namespace UnitTestSpreadSheet
{
	/// <summary>
	///[version2] For  testing Interpreter Pattern 
	/// </summary>
	[TestClass]
	public class UnitTest2_Interpreter
	{
		[TestMethod]

		public void TestInterpreter1()
		{
			Console.WriteLine();
			var result = string.Empty;
			result = Translator.GetResultByInterpreter("C 20 4");
			Console.WriteLine(result);
			result = Translator.GetResultByInterpreter("N 1 2 2");
			Console.WriteLine(result);
			result = Translator.GetResultByInterpreter("N 1 3 4");
			Console.WriteLine(result);
			result = Translator.GetResultByInterpreter("S 1 2 1 3 1 4");
			Console.WriteLine(result);
		}
		[TestMethod]
		public void TestInterpreter1MutilpleRows()
		{
			var result = Translator.GetResultByInterpreter("C 5 4");
			Console.WriteLine(result);
			result = Translator.GetResultByInterpreter("N 1 1 1");
			Console.WriteLine(result);
			result = Translator.GetResultByInterpreter("N 1 2 2");
			Console.WriteLine(result);

			Console.WriteLine(result);
			result = Translator.GetResultByInterpreter("N 2 1 21");
			Console.WriteLine(result);
			result = Translator.GetResultByInterpreter("N 2 2 22");
			Console.WriteLine(result);

			result = Translator.GetResultByInterpreter("S 2 1 2 2 1 4");
			Console.WriteLine(result);
		}
	}
}
