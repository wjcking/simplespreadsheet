﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSpreadsheet;

namespace UnitTest
{
	[TestClass]
	public class Test1
	{
		[TestMethod]
		public void TestCount()
		{ 
			
			Console.WriteLine("---------------------------------------------------------------".Length.ToString());
		}
		[TestMethod]
		public void TestCount2()
		{
			Console.WriteLine("|  2                                                          |".Length.ToString());
		}
		[TestMethod]
		public void TestInput()
		{ 
			Translator.GetResult("C 20 4");
		}
		[TestMethod]
		public void TestInputEmpty()
		{
			 
			Translator.GetResult(string.Empty);
		}
		[TestMethod]
		public void TestSheetsEmpty()
		{
 
		}
	 
		[TestMethod]
		public void Test2dArray()
		{
			int[,] vectors= new int[3,3];
			vectors[0,0] = 334;
			foreach(var v in vectors)
			{
				Console.WriteLine(v.ToString( )); 
			}
//			for (int i=0;i<vectors.Length;i++)
//			{
//				
//				Console.WriteLine(i.ToString() +":"+   vectors[i][0].ToString());
//			}
		}
		
		[TestMethod]
		public void TestConversion()
		{
			var command = "S d 2 2 1 3 3";
			string commandPrefix = command.Substring(0, 1).ToUpper();
			var splited = command.Substring(2).Split(" ".ToString().ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
		
			var coordinates = new int[splited.Length];
			for (int i=0;i< splited.Length;i++)
			{ 
				int.TryParse(splited[i], out coordinates[i]);
				
				if (coordinates[i] == 0)
					throw new Exception("params:" + (i+1).ToString() + " out of boundary");
				else
					--coordinates[i];
				Console.WriteLine(coordinates[i]);
			}
			
			 
		}
	}
}
