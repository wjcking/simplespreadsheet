﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSpreadsheet;

namespace UnitTestSpreadSheet
{
	/// <summary>
	///[version1] Test for outputing the sheets
	/// </summary>
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
 
		public void Test1()
		{
		 
			Console.WriteLine();
			var result = string.Empty;
			result = Translator.GetResult("C 20 4");
			Console.WriteLine(result);
			result = Translator.GetResult("N 1 2 2");
			Console.WriteLine(result);
			result = Translator.GetResult("N 1 3 4");
			Console.WriteLine(result);
			result = Translator.GetResult("S 1 2 1 3 1 4");
			Console.WriteLine(result);
		}

		/// <summary>
		/// another table
		/// </summary>
		[TestMethod]
		public void Test2()
		{
			Console.WriteLine();
			var result = string.Empty;
			result = Translator.GetResult("C 12 6");
			Console.WriteLine(result);
			result = Translator.GetResult("N 1 1 1");
			Console.WriteLine(result);
			result = Translator.GetResult("N 1 2 2");
			Console.WriteLine(result);
			result = Translator.GetResult("N 1 3 3");
			Console.WriteLine(result);
			result = Translator.GetResult("S 1 1 1 3 1 4");
			Console.WriteLine(result);
		}
		/// <summary>
		/// merged
		/// </summary>
		[TestMethod]
		public void Test3()
		{
			Test1();
			Test2();
		}
		
		[TestMethod]
		public void TestMutilpleRows()
		{
			var result = Translator.GetResult("C 5 4");
			Console.WriteLine(result);
			result = Translator.GetResult("N 1 1 1");
			Console.WriteLine(result);
			result = Translator.GetResult("N 1 2 2");
			Console.WriteLine(result);

			Console.WriteLine(result);
			result = Translator.GetResult("N 2 1 21");
			Console.WriteLine(result);
			result = Translator.GetResult("N 2 2 22");
			Console.WriteLine(result);

			result = Translator.GetResult("S 2 1 2 2 1 4");
			Console.WriteLine(result);
		}
	}
}
